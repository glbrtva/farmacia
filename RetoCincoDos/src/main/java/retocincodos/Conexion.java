package retocincodos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Conexion {

    private String URL = "Reto5.db";
    private PreparedStatement stmt = null;
    private ResultSet rs = null;
    Connection conexion = null;
    private Connection con;

    public Conexion() {
        try {
            conexion = DriverManager.getConnection("jdbc:sqlite:" + URL);
            if (conexion != null) {
                System.out.println("Conexion exitosa");
            }
        } catch (Exception e) {
            System.out.println("Conexion fallida");
        }
    }

    public void agregarDatos(String Id, String Nombre, Double Temperatura, Double ValorBase) {
        try {
            stmt = conexion.prepareStatement("INSERT INTO Producto (Id, Nombre, Temperatura, ValorBase) VALUES (?,?,?,?);");
            stmt.setString(1, Id);
            stmt.setString(2, Nombre);
            stmt.setDouble(3, Temperatura);
            stmt.setDouble(4, ValorBase);
            stmt.executeUpdate();
            System.out.println("Agregado");
        } catch (Exception e) {
            System.out.println("No se pudo agregar");
        }
    }

    public void modificarDatos(String Id, String Nombre, Double Temperatura, Double ValorBase) {
        try {
            stmt = conexion.prepareStatement("UPDATE Producto SET Nombre = ?, Temperatura = ?, ValorBase = ? WHERE Id = ?;");
            stmt.setString(4, Id);
            stmt.setString(1, Nombre);
            stmt.setDouble(2, Temperatura);
            stmt.setDouble(3, ValorBase);
            stmt.executeUpdate();
            System.out.println("Modificado");
        } catch (Exception e) {
            System.out.println("No se pudo modificar");
        }
    }

    public void eliminarDatos(String codigo) {
        try {
            stmt = conexion.prepareStatement("DELETE FROM Producto WHERE Id = ?;");
            stmt.setString(1, codigo);
            stmt.executeUpdate();
            System.out.println("Eliminado");
        } catch (Exception e) {
            System.out.println("No se pudo eliminar");
        }
    }

    public ResultSet buscarDatos(String codigo, String Nombre) {
        try {
            stmt = conexion.prepareStatement("SELECT Id, Nombre FROM Producto;");
            rs = stmt.executeQuery();
        } catch (Exception e) {
            System.out.println("Datos no encontrados");
        }
        return rs;
    }

    public List<Producto> listarProducto() {
        List<Producto> datos = new ArrayList<>();
        try {
            stmt = con.prepareStatement("SELECT * FROM producto;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                Producto p = new Producto(rs.getString("Id"), rs.getString("Nombre"), rs.getDouble("Temperatura"), rs.getDouble("ValorBase"));
                datos.add(p);
            }
        } catch (Exception e) {
            System.out.println("No se han descargado los datos.");
            return null;
        }
        return datos;
    }

}
